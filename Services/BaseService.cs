﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Services
{
    public class BaseService 
    {
        private readonly string _connectionName = "library";
        public MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConfigurationManager.ConnectionStrings[_connectionName].ConnectionString);
        }
    }
}
